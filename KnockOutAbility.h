// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NativeGameplayTags.h"
#include "AbilitySystem/Abilities/BaseGameplayAbility.h"
#include "KnockOutAbility.generated.h"

UE_DECLARE_GAMEPLAY_TAG_EXTERN(TAG_GameplayAbility_Player_KnockOut);

/**
 * 
 */
UCLASS()
class UNREAL4_API UKnockOutAbility : public UBaseGameplayAbility
{
	GENERATED_BODY()

public:
	UKnockOutAbility();

	static constexpr float KnockOutSeconds = 60.f;

	static constexpr float DefaultRestoredHealth = 60.f;

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

protected:
	FTimerHandle DyingTimerHandle;
};
