// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "Components/ArmorComponent.h"
#include "Components/PlayerHealthComponent.h"
#include "Pickup/ArmorInterface.h"
#include "Player/PlayerControllerInterface.h"
#include "UObject/Interface.h"
#include "Weapon/WeaponInterface.h"
#include "Weapon/Ammo/AmmoInterface.h"
#include "CharacterInterface.generated.h"

// This class does not need to be modified.
UINTERFACE()
class UCharacterInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class UNREAL4_API ICharacterInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual IPlayerControllerInterface* GetPlayerController() = 0;
	virtual UArmorComponent* GetArmorComponent() = 0;
	virtual UPlayerHealthComponent* GetHealthComponent() = 0;
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const = 0;
	
	virtual bool PickupAmmo(IAmmoInterface* Ammo) = 0;
	virtual void PickupWeapon(IWeaponInterface* InWeapon) = 0;
	virtual void PickupArmor(IArmorInterface* InArmor) = 0;
	
	virtual IWeaponInterface* GetCurrentWeapon() const = 0;

	virtual void StartReloadWeapon() = 0;
	virtual void CancelReloadWeapon() = 0;

	virtual void SetKnockOutState(const bool bEnabled) = 0;
	virtual void RescuePlayer(ICharacterInterface* KnockedOutPlayer) = 0;
};
