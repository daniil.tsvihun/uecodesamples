// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NativeGameplayTags.h"
#include "AbilitySystem/Abilities/BaseGameplayAbility.h"
#include "SelfDestroyAbility.generated.h"

UE_DECLARE_GAMEPLAY_TAG_EXTERN(TAG_GameplayAbility_Player_SelfDestroy);

/**
 * 
 */
UCLASS()
class UNREAL4_API USelfDestroyAbility : public UBaseGameplayAbility
{
	GENERATED_BODY()

public:
	USelfDestroyAbility();

	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

protected:
	UPROPERTY(EditDefaultsOnly)
	float DamageRadius = 1250;

	UPROPERTY(EditDefaultsOnly)
	float Damage = 60;

	UPROPERTY(EditDefaultsOnly)
	float TimerSeconds = 5;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<UGameplayEffect> GamePlayEffectToApply;

	FTimerHandle TimerHandle;

	void SelfDestroy();
};
