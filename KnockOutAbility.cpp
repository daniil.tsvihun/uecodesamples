// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Abilities/Player/KnockOutAbility.h"

#include "Abilities/BioSurge.h"
#include "AbilitySystem/Abilities/Player/HoldWeaponAbility.h"
#include "AbilitySystem/Abilities/Player/ReloadAbility.h"
#include "AbilitySystem/Abilities/Player/ShootAbility.h"
#include "AbilitySystem/Abilities/Player/SprintAbility.h"
#include "AbilitySystem/Abilities/Player/Aim/AimAbility.h"
#include "Characters/CharacterInterface.h"
#include "Kismet/GameplayStatics.h"

UE_DEFINE_GAMEPLAY_TAG(TAG_GameplayAbility_Player_KnockOut, "GameplayAbility.Player.KnockOut");

UKnockOutAbility::UKnockOutAbility()
{
	AbilityTags = FGameplayTagContainer(TAG_GameplayAbility_Player_KnockOut);

	BlockAbilitiesWithTag.AddTag(TAG_GameplayAbility_Player_Sprint);
	BlockAbilitiesWithTag.AddTag(TAG_GameplayAbility_Player_Shoot);
	BlockAbilitiesWithTag.AddTag(TAG_GameplayAbility_Player_BioSurge);
	BlockAbilitiesWithTag.AddTag(TAG_GameplayAbility_Player_Aim);
	BlockAbilitiesWithTag.AddTag(TAG_GameplayAbility_Player_Reload);
	BlockAbilitiesWithTag.AddTag(TAG_GameplayAbility_Player_HoldWeapon);
}

void UKnockOutAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
	if(ICharacterInterface* OwningCharacter = Cast<ACharacterBase>(GetOwningActor()))
	{
		OwningCharacter->SetKnockOutState(true);
	}

	GetWorld()->GetTimerManager().SetTimer(DyingTimerHandle, [&]()
	{
		AActor* OwningActor = GetOwningActor();
		if (!IsValid(OwningActor))
		{
			return;
		}
		UGameplayStatics::ApplyPointDamage(OwningActor, 1, FVector(), FHitResult(ForceInit), nullptr, nullptr, UDamageType::StaticClass());
	}, KnockOutSeconds, false);
}

void UKnockOutAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	GetWorld()->GetTimerManager().ClearTimer(DyingTimerHandle);
	if(ICharacterInterface* OwningCharacter = Cast<ICharacterInterface>(GetOwningActor()))
	{
		OwningCharacter->SetKnockOutState(false);

		if (OwningCharacter->GetHealthComponent())
		{
			OwningCharacter->GetHealthComponent()->SetHealth(DefaultRestoredHealth);
			OwningCharacter->GetHealthComponent()->bCanKnockOut = true;
		}

		if (OwningCharacter->GetPlayerController())
		{
			OwningCharacter->GetPlayerController()->DeactivateKnockOutWidget();
		}
	}
	
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}
