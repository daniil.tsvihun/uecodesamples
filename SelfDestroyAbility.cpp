// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Abilities/Player/SelfDestroyAbility.h"

#include "EngineUtils.h"
#include "Characters/CharacterBase.h"
#include "Kismet/GameplayStatics.h"

UE_DEFINE_GAMEPLAY_TAG(TAG_GameplayAbility_Player_SelfDestroy, "GameplayAbility.Player.SelfDestroy");

USelfDestroyAbility::USelfDestroyAbility()
{
	AbilityTags = FGameplayTagContainer(TAG_GameplayAbility_Player_SelfDestroy);
}

void USelfDestroyAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	if (ICharacterInterface* CharOwner = Cast<ICharacterInterface>(GetOwningActor()))
	{
		if (CharOwner->GetPlayerController())
		{
			CharOwner->GetPlayerController()->StartSelfDestroyTimer(TimerSeconds);
		}
	}

	GetWorld()->GetTimerManager().SetTimer(TimerHandle, [&]()
	{
		SelfDestroy();
	}, TimerSeconds, false);
}

void USelfDestroyAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
	
	if (bWasCancelled)
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
		if (ICharacterInterface* CharOwner = Cast<ICharacterInterface>(GetOwningActor()))
		{
			if (CharOwner->GetPlayerController())
			{
				CharOwner->GetPlayerController()->StopSelfDestroyTimer();
			}
		}
	}
}

void USelfDestroyAbility::SelfDestroy()
{
	AActor* Owner = GetOwningActor();

	if (!IsValid(Owner))
	{
		return;
	}

	if (ICharacterInterface* CharOwner = Cast<ICharacterInterface>(Owner))
	{
		CharOwner->GetPlayerController()->DeactivateKnockOutWidget();
	}

	for(FActorIterator It(GetWorld()); It; ++It )
	{
		if (It->IsA(ACharacterBase::StaticClass()) && IsValid(*It))
		{
			if(FVector::Distance(Owner->GetActorLocation(), It->GetActorLocation()) <= DamageRadius)
			{
				UGameplayStatics::ApplyPointDamage(Cast<AActor>(*It), Damage, Owner->GetActorLocation(), FHitResult(ForceInit), It->GetInstigatorController(), Owner, UDamageType::StaticClass());
			}
		}
	}
}
